// Add (hard code) an orbiting (point or directional) light to the scene. Light
// the scene using the Blinn-Phong Lighting Model.
//
// Uniforms:
uniform mat4 view;
uniform mat4 proj;
uniform float animation_seconds;
uniform bool is_moon;
// Inputs:
in vec3 sphere_fs_in;
in vec3 normal_fs_in;
in vec4 pos_fs_in; 
in vec4 view_pos_fs_in; 
// Outputs:
out vec3 color;
// expects: PI, blinn_phong
void main()
{
  /////////////////////////////////////////////////////////////////////////////
  // Replace with your code
  
  vec3 ka, kd, ks;
  float p;
  if (is_moon){
    ka = vec3(0.1, 0.1, 0.1);
    kd = vec3(0.5, 0.5, 0.5);
    ks = vec3(0.8, 0.8, 0.8);
    p = 1000;
  } else {
    ka = vec3(0.1, 0.1, 0.1);
    kd = vec3(0.196,0.258,0.907);
    ks = vec3(0.8, 0.8, 0.8);
    p = 1000;
  }
  vec3 n = normalize(normal_fs_in);
  vec3 v = -normalize(view_pos_fs_in.xyz/view_pos_fs_in.w);

  float theta = -(M_PI/ 2) * animation_seconds;
  mat4 rotate_about_y = mat4(
    cos(theta),0,-sin(theta),0,
    0,1,0,0,
    sin(theta),0,cos(theta),0,
    0,0,0,1);
  vec4 lightsource = view * rotate_about_y*vec4(3,1,4,1);
  vec3 l = normalize((lightsource.xyz/lightsource.w) - (view_pos_fs_in.xyz/view_pos_fs_in.w));

  color = blinn_phong(ka, kd, ks, p, n, v, l);
  /////////////////////////////////////////////////////////////////////////////
}
