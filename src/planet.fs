// Generate a procedural planet and orbiting moon. Use layers of (improved)
// Perlin noise to generate planetary features such as vegetation, gaseous
// clouds, mountains, valleys, ice caps, rivers, oceans. Don't forget about the
// moon. Use `animation_seconds` in your noise input to create (periodic)
// temporal effects.
//
// Uniforms:
uniform mat4 view;
uniform mat4 proj;
uniform float animation_seconds;
uniform bool is_moon;
// Inputs:
in vec3 sphere_fs_in;
in vec3 normal_fs_in;
in vec4 pos_fs_in; 
in vec4 view_pos_fs_in; 
// Outputs:
out vec3 color;
// expects: model, blinn_phong, bump_height, bump_position,
// improved_perlin_noise, tangent
void main()
{
  /////////////////////////////////////////////////////////////////////////////
  // Replace with your code

  vec3 T, B;
  tangent(normalize(sphere_fs_in), T, B);
  float epsilon = 0.0001;
  vec3 pb = bump_position(is_moon, sphere_fs_in);
  vec3 dpdT = (bump_position(is_moon, sphere_fs_in + T * epsilon) - pb) / epsilon;
  vec3 dpdB = (bump_position(is_moon, sphere_fs_in + B * epsilon) - pb) / epsilon;
  vec3 n = normalize(cross(dpdT, dpdB));
  float factor = 0.5 * sin(animation_seconds/2) + 0.5;
  
  if (!is_moon){  
    n = mix(normalize(normal_fs_in), n, factor);
  }


  vec3 ka, kd, ks;
  float p;
  if (is_moon){
    ka = vec3(0.1, 0.1, 0.1);
    kd = vec3(0.5, 0.5, 0.5);
    ks = vec3(0.8, 0.8, 0.8);
    p = 1000;
  } else {
    ka = vec3(0.5, 0.5, 0.5);
    float height_factor = min(1,max(0, 10*factor*bump_height(is_moon, sphere_fs_in)));
    //height_factor = 1;
    vec3 colour1 = vec3(0.066,0.176,0.450);
    vec3 colour2 = vec3(0.654,0.874,0.188);
    kd = colour1*(1-height_factor) + colour2 * height_factor;

    float cloud_factor = improved_perlin_noise(vec3 (animation_seconds/5))+0.5;
    cloud_factor = max(0, min(1, cloud_factor));
    float k1 = 15*cloud_factor;
    float k2 = 7*cloud_factor;
    float w = 1;
    float noise;
    noise = ((improved_perlin_noise(k2*sphere_fs_in+cloud_factor)) * k1);
    noise = noise *(1+sin(k1*sphere_fs_in.y+improved_perlin_noise(k2*sphere_fs_in))/w)/2;;
    noise = max(0, min(1, noise));
    kd += noise;
    
    ks = vec3(0.8, 0.8, 0.8);
    p = 1000;
  }
  vec3 v = -normalize(view_pos_fs_in.xyz/view_pos_fs_in.w);

  float theta = -(M_PI/ 2) * animation_seconds;
  mat4 rotate_about_y = mat4(
    cos(theta),0,-sin(theta),0,
    0,1,0,0,
    sin(theta),0,cos(theta),0,
    0,0,0,1);
  vec4 lightsource = view * rotate_about_y*vec4(3,1,4,1);
  vec3 l = normalize((lightsource.xyz/lightsource.w) - (view_pos_fs_in.xyz/view_pos_fs_in.w));

  color = blinn_phong(ka, kd, ks, p, n, v, l);
  /////////////////////////////////////////////////////////////////////////////
}
