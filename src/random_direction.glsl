// Generate a pseudorandom unit 3D vector
// 
// Inputs:
//   seed  3D seed
// Returns psuedorandom, unit 3D vector drawn from uniform distribution over
// the unit sphere (assuming random2 is uniform over [0,1]²).
//
// expects: random2.glsl, PI.glsl
vec3 random_direction( vec3 seed)
{
  /////////////////////////////////////////////////////////////////////////////
  // Replace with your code 
  vec2 random_num = random2(seed);
  float phi = M_PI* 2 *random_num.x;
  float theta = M_PI * random_num.y;
  float r = 1.0;
  float x = r*sin(theta)*cos(phi);
  float y = r*sin(theta)*sin(phi);
  float z = r*cos(theta);

  return normalize(vec3(x,y,z));
  /////////////////////////////////////////////////////////////////////////////
}
