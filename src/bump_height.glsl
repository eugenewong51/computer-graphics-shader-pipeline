// Create a bumpy surface by using procedural noise to generate a height (
// displacement in normal direction).
//
// Inputs:
//   is_moon  whether we're looking at the moon or centre planet
//   s  3D position of seed for noise generation
// Returns elevation adjust along normal (values between -0.1 and 0.1 are
//   reasonable.
float bump_height( bool is_moon, vec3 s)
{
  /////////////////////////////////////////////////////////////////////////////
  // Replace with your code 
  if (is_moon){
    return 0.02*smooth_heaviside(improved_perlin_noise(s*5), 5);
  } else {
    float layer1 = 0.08*smooth_heaviside(improved_perlin_noise(s*2), 30);
    float layer2;
    if (layer1>0.02){
      layer2 = 0.03*smooth_heaviside(improved_perlin_noise(s*4), 13);
    } else if (layer1 <0){
      layer2 = 0.05*smooth_heaviside(improved_perlin_noise(s*2), 5);
    }
    return layer1 + layer2;
  }
  /////////////////////////////////////////////////////////////////////////////
}
