// Set the pixel color to blue or gray depending on is_moon.
//
// Uniforms:
uniform bool is_moon;
// Outputs:
out vec3 color;
void main()
{
  /////////////////////////////////////////////////////////////////////////////
  // Replace with your code:
  if (is_moon){
    color = vec3(0.533, 0.453, 0.524);
  } else {
    color = vec3(0.196,0.258,0.907);
  }
  
  /////////////////////////////////////////////////////////////////////////////
}
