// Given a 3d position as a seed, compute a smooth procedural noise
// value: "Perlin Noise", also known as "Gradient noise".
//
// Inputs:
//   st  3D seed
// Returns a smooth value between (-1,1)
//
// expects: random_direction, smooth_step
float perlin_noise( vec3 st) 
{
  /////////////////////////////////////////////////////////////////////////////
  // Replace with your code 
  // https://en.wikipedia.org/wiki/Perlin_noise
  // https://www.scratchapixel.com/lessons/procedural-generation-virtual-worlds/perlin-noise-part-2

  //grid cube coordinates
  int x0 = int(floor(st.x));
  int x1 = x0+1;
  int y0 = int(floor(st.y));
  int y1 = y0+1;
  int z0 = int(floor(st.z));
  int z1 = z0+1;

  vec3 p000 = vec3(x0, y0, z0); 
  vec3 p100 = vec3(x1, y0, z0); 
  vec3 p010 = vec3(x0, y1, z0); 
  vec3 p110 = vec3(x1, y1, z0); 
  vec3 p001 = vec3(x0, y0, z1); 
  vec3 p101 = vec3(x1, y0, z1); 
  vec3 p011 = vec3(x0, y1, z1); 
  vec3 p111 = vec3(x1, y1, z1);

  // interpolation weights
  float sx = st.x - float(x0);
  float sy = st.y - float(y0);
  float sz = st.z - float(z0);

  // gradient
  vec3 g000 = random_direction(p000);
  vec3 g100 = random_direction(p100);
  vec3 g010 = random_direction(p010);
  vec3 g110 = random_direction(p110);
  vec3 g001 = random_direction(p001);
  vec3 g101 = random_direction(p101);
  vec3 g011 = random_direction(p011);
  vec3 g111 = random_direction(p111);

  //distance vector
  vec3 d000 = st - p000;
  vec3 d100 = st - p100;
  vec3 d010 = st - p010;
  vec3 d110 = st - p110;
  vec3 d001 = st - p001;
  vec3 d101 = st - p101;
  vec3 d011 = st - p011;
  vec3 d111 = st - p111;

  //dot product
  float n000 = dot(d000, g000);
  float n100 = dot(d100, g100);
  float n010 = dot(d010, g010);
  float n110 = dot(d110, g110);
  float n001 = dot(d001, g001);
  float n101 = dot(d101, g101);
  float n011 = dot(d011, g011);
  float n111 = dot(d111, g111);

  vec3 s_step = smooth_step(vec3(sx, sy, sz));

  float a = mix(n000, n100, s_step.x);
  float b = mix(n010, n110, s_step.x);
  float c = mix(n001, n101, s_step.x);
  float d = mix(n011, n111, s_step.x);

  float e = mix(a, b, s_step.y);
  float f = mix(c, d, s_step.y);

  float g = mix(e, f, s_step.z);

  return g;
  /////////////////////////////////////////////////////////////////////////////
}

